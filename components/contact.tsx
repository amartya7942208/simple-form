"use client";
import { useState } from "react";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
} from "@/components/ui/dialog";
import { Button } from "./ui/button";

export default function Contact() {
  const [showModal, setShowModal] = useState(false);
  const [formSubmitted, setFormSubmitted] = useState(false);

  const submitForm = async (event: any) => {
    event.preventDefault();

    try {
      const response = await fetch(
        "https://docs.google.com/forms/d/e/1FAIpQLSd45OV_7m5JpNs1vjBQbTrhDgBE6xgEQUxQjikSc-h-dFsTKA/formResponse",
        {
          method: "POST",
          body: new FormData(event.target),
          mode: "no-cors",
        }
      );

      setFormSubmitted(true);
      setShowModal(true);
      event.target.reset();
    } catch (error) {
      console.error("Error:", error);
    }
  };
  return (
    <div className=" mx-auto max-w-7xl py-8 ">
      <div className="md:flex md:flex-row  justify-around gap px-6 items-center ">
        <div className=" mt-12 ">
          <div className="text-center mb-8">
            <h1 className="dark:text-white text-3xl font-bold text-gray-800 sm:text-4xl">
              Contact us
            </h1>
            <p className="dark:text-gray-400 mt-1 text-gray-600">
              Tell us your story and we&apos;ll be in touch.
            </p>
          </div>

          <div>
            {/* <!-- Form --> */}
            <form id="contactForm" onSubmit={submitForm}>
              <div className="grid gap-4 lg:gap-6 ">
                {/* <!-- Grid --> */}
                <div className="grid grid-cols-1 gap-4 sm:grid-cols-2 lg:gap-6">
                  <div>
                    <label
                      htmlFor="entry.2005620554"
                      className="dark:text-white mb-2 block text-sm font-medium text-gray-700"
                    >
                      Name
                    </label>
                    <input
                      required
                      type="text"
                      name="entry.2005620554"
                      className="dark:bg-slate-900 dark:border-gray-700 dark:text-gray-400 dark:focus:ring-gray-600 block w-full rounded-lg border border-gray-200 px-4 py-3 text-sm focus:border-blue-500 focus:ring-blue-500 disabled:pointer-events-none disabled:opacity-50"
                    />
                  </div>

                  <div>
                    <label
                      htmlFor="entry.1166974658"
                      className="dark:text-white mb-2 block text-sm font-medium text-gray-700"
                    >
                      Phone
                    </label>
                    <input
                      type="text"
                      name="entry.1166974658"
                      className="dark:bg-slate-900 dark:border-gray-700 dark:text-gray-400 dark:focus:ring-gray-600 block w-full rounded-lg border border-gray-200 px-4 py-3 text-sm focus:border-blue-500 focus:ring-blue-500 disabled:pointer-events-none disabled:opacity-50"
                    />
                  </div>
                </div>
                {/* <!-- End Grid --> */}

                <div>
                  <label
                    htmlFor="entry.1045781291"
                    className="dark:text-white mb-2 block text-sm font-medium text-gray-700"
                  >
                    Email
                  </label>
                  <input
                    type="email"
                    name="entry.1045781291"
                    id="hs-work-email-hire-us-2"
                    autoComplete="email"
                    className="dark:bg-slate-900 dark:border-gray-700 dark:text-gray-400 dark:focus:ring-gray-600 block w-full rounded-lg border border-gray-200 px-4 py-3 text-sm focus:border-blue-500 focus:ring-blue-500 disabled:pointer-events-none disabled:opacity-50"
                  />
                </div>

                {/* <!-- Grid --> */}
                <div className="grid grid-cols-1 gap-4 sm:grid-cols-2 lg:gap-6">
                  <div>
                    <label
                      htmlFor="entry.1065046570"
                      className="dark:text-white mb-2 block text-sm font-medium text-gray-700"
                    >
                      Address
                    </label>
                    <input
                      type="text"
                      name="entry.1065046570"
                      id="hs-company-hire-us-2"
                      className="dark:bg-slate-900 dark:border-gray-700 dark:text-gray-400 dark:focus:ring-gray-600 block w-full rounded-lg border border-gray-200 px-4 py-3 text-sm focus:border-blue-500 focus:ring-blue-500 disabled:pointer-events-none disabled:opacity-50"
                    />
                  </div>

                  <div>
                    <label
                      htmlFor="entry.989589165"
                      className="dark:text-white mb-2 block text-sm font-medium text-gray-700"
                    >
                      Company (Optional)
                    </label>
                    <input
                      type="text"
                      name="entry.989589165"
                      className="dark:bg-slate-900 dark:border-gray-700 dark:text-gray-400 dark:focus:ring-gray-600 block w-full rounded-lg border border-gray-200 px-4 py-3 text-sm focus:border-blue-500 focus:ring-blue-500 disabled:pointer-events-none disabled:opacity-50"
                    />
                  </div>
                </div>
                {/* <!-- End Grid --> */}

                <div>
                  <label
                    htmlFor="entry.839337160"
                    className="dark:text-white mb-2 block text-sm font-medium text-gray-700"
                  >
                    How can we assist you?
                  </label>
                  <textarea
                    required
                    id="hs-about-hire-us-2"
                    name="entry.839337160"
                    className="dark:bg-slate-900 dark:border-gray-700 dark:text-gray-400 dark:focus:ring-gray-600 block w-full rounded-lg border border-gray-200 px-4 py-3 text-sm focus:border-blue-500 focus:ring-blue-500 disabled:pointer-events-none disabled:opacity-50"
                  ></textarea>
                </div>
              </div>
              {/* <!-- End Grid --> */}

              <div className="mt-6 grid">
                <button
                  type="submit"
                  // onclick="submitForm()"
                  className="dark:focus:ring-1 dark:focus:ring-gray-600 inline-flex w-full items-center justify-center gap-x-2 rounded-lg border-2 border-zinc-200 bg-blue-600 px-4 py-3 text-sm font-semibold text-white hover:bg-blue-700 disabled:pointer-events-none disabled:opacity-50 dark:focus:outline-none"
                >
                  Send inquiry
                </button>
              </div>

              <div className="mt-3 text-center">
                <p className="text-sm text-gray-500">
                  We&apos;ll get back to you in{" "}
                  <span className="font-semibold text-blue-500">
                    Within 24 hours
                  </span>{" "}
                </p>
              </div>
            </form>

            <Dialog open={showModal} onOpenChange={() => setShowModal(false)}>
              <DialogContent>
                <DialogHeader>
                  <DialogTitle>Thank You for Applying!</DialogTitle>
                  <DialogDescription>
                    Our team is reviewing it and will reach out soon with
                    personalized details. For urgent inquiries, use our contact
                    number or visit our office.
                    <br />
                    {/* <p className="font-medium">
                      +91 7003950585,  +91 4003960595
                    </p>
                    <p className="font-medium">Behala, Kolkata.</p> */}
                  </DialogDescription>
                </DialogHeader>
              </DialogContent>
            </Dialog>

            {/* <!-- End Form --> */}
          </div>
        </div>

        {/* Box */}
        <div>
          <div className="bg-stone-50 w-[20rem] h-[15rem]  mx-auto rounded-2xl mt-12 p-10 ">
            <p className="text-black font-semibold text-xl mb-4">
              Get in Touch
            </p>

            <p className="text-blue-700 font-medium text-md ">
              Address &nbsp;
              <span className="text-black text-sm">
                Anjali Greens, Block-2, Flat-4A, Arunachal, Hatiara
              </span>
            </p>

            <p className="text-blue-700 font-medium text-md ">
              Call us at &nbsp;
              <span className="text-black text-sm">
                +91 7003960595
              </span>
            </p>
          </div>

          <div className="bg-stone-50 w-[20rem]  h-[15rem]  mx-auto rounded-2xl mt-12 p-10 ">
            <p className="text-black font-semibold text-xl mb-4">
              Get in Touch
            </p>
            <a
              href="mailto:cosmicpestsolutions@gmail.com"
              type="button"
              className="bg-blue-500 text-white font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2"
            >
              Hello
            </a>
            <a
              href="/change"
              type="button"
              className="bg-blue-500 text-white font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2"
            >
              hahaha
            </a>  
          </div>
        </div>
      </div>
    </div>
  );
}
